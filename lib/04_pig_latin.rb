def vowel str
  str.downcase =~ /[aeiou]/
end

def translate input
  arr = input.split
  output = []
  arr.each do |word|
    vowel_one = vowel word
    if vowel_one == 0
      output << "#{word}ay"
    elsif word[0..1].downcase == "qu"
      output << "#{word[2..-1]}#{word[0..1]}ay"
    elsif word[0..2].downcase == "squ"
      output << "#{word[3..-1]}#{word[0..2]}ay"
    else
      output << "#{word[vowel_one..-1]}#{word[0...vowel_one]}ay"
    end
  end
  output.join " "
end
