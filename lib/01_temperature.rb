def ftoc df
  (df.to_f - 32) * 5 / 9
end

def ctof dc
  (dc.to_f * 9 / 5) + 32
end
