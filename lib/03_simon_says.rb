LITTLE_WORDS = ["and", "over", "the"]

def echo input
  input
end

def shout input
  input.upcase
end

def repeat input, reps = 2
  output = []
  reps.times { output << input }
  output.join " "
end

def start_of_word input, len
  input[0..len - 1]
end

def first_word input
  input.split[0]
end

def titleize input
  arr = input.split
  output = []
  arr.each.with_index do |word, index|
    if LITTLE_WORDS.include?(word) && index != 0
      output << word
    else
      output << word.capitalize
    end
  end
  output.join " "
end
